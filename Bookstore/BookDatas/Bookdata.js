const bookData = [
    {
      id: '1',
      title: 'To all the boys I have loved before',
      author: 'Jenny han',
      price: 'Nu:750',
    },
    {
      id: '2',
      title: 'After',
      author: 'Anna Todd',
      price: 'Nu:350',
    },
    {
        id: '3',
        title: 'After I fall',
        author: 'Ana Todd',
        price: 'Nu:1000',
      },
      {
        id: '4',
        title: 'Five Feet Apart',
        author: 'Mikki Daunghtry',
        price: 'Nu:750',
      },
      {
        id: '5',
        title: 'Fault in our start',
        author: 'John Green',
        price: 'Nu:400',
      },

  ];
  export default bookData;