import React, { useState } from 'react';
import { View, Text, FlatList, TouchableOpacity, Modal, Button, StyleSheet } from 'react-native';
import bookData from "../BookDatas/Bookdata";

const BookList = () => {
  const [selectedBook, setSelectedBook] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity 
        onPress={() => { 
          setSelectedBook(item); 
          setIsModalVisible(true);
        }} 
        style={styles.itemContainer}
      > 
        <View style={styles.bookContainer}>
          <Text style={styles.title}>{item.title}</Text> 
          <Text style={styles.author}>{item.author}</Text> 
          <Text style={styles.price}>{item.price}</Text> 
          <Button title="Read" onPress={() => { console.log('Read button pressed'); }} />
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList 
        data={bookData} 
        keyExtractor={(item) => item.id} 
        renderItem={renderItem} 
        numColumns={2}
        key={isModalVisible ? 'modal' : 'list'}
        style={styles.list}
      />
      <Modal 
        visible={isModalVisible} 
        animationType="slide" 
        transparent={true} 
      > 
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text style={styles.modalTitle}>{selectedBook ? selectedBook.title : ''}</Text> 
            <Text style={styles.modalAuthor}>{selectedBook ? selectedBook.author : ''}</Text> 
            <Text style={styles.modalPrice}>{selectedBook ? selectedBook.price : ''}</Text> 
            <Button title="Close" onPress={() => { setIsModalVisible(false); setSelectedBook(null); }} />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
    paddingTop: 16,
  },
  list: {
    paddingHorizontal: 16,
  },
  itemContainer: {
    width: '50%',
    padding: 8,
  },
  bookContainer: {
    backgroundColor: '#fff',
    padding: 16,
    borderRadius: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 6,
  },
  title: {
    fontSize: 20, 
    fontWeight: 'bold',
    color: '#333',
  },
  author: {
    fontSize: 18,
    color: '#555',
  },
  price: {
    fontSize: 18, 
    color: 'green',
  },
  modalContainer: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#fff', 
    padding: 20, 
    borderRadius: 12, 
    width: '80%',
  },
  modalTitle: {
    fontSize: 28, 
    fontWeight: 'bold',
    marginBottom: 10,
    color: '#333',
  },
  modalAuthor: {
    fontSize: 24,
    marginBottom: 10,
    color: '#555',
  },
  modalPrice: {
    fontSize: 24, 
    color: 'green',
    marginBottom: 20,
  },
});

export default BookList;
