import React from 'react';
import { View, StyleSheet } from 'react-native';
import BookList from "./Main/Booklist"

export default function App() {
  return (
    <View style={styles.container}>
      <BookList />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    padding: 20,
  },
});